package com.yonder.myproject;

import javax.ws.rs.Path;

import javax.ws.rs.core.MediaType;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;

@Path("helloworld")
public class Hello {
	public static final String MESSAGE = "hello";
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getHello() {
		return MESSAGE;
	}
	
}
