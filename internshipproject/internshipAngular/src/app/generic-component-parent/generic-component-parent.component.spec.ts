import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericComponentParentComponent } from './generic-component-parent.component';

describe('GenericComponentChildComponent', () => {
  let component: GenericComponentParentComponent;
  let fixture: ComponentFixture<GenericComponentParentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericComponentParentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericComponentParentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
