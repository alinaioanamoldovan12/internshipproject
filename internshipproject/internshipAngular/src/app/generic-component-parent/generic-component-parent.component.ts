import { Component, OnInit, Input } from '@angular/core';
import {ViewEncapsulation} from '@angular/core';
@Component({
  selector: 'generic-component-parent',
  templateUrl: './generic-component-parent.component.html',
  styleUrls: ['./generic-component-parent.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GenericComponentParentComponent implements OnInit {

  @Input() data:string;
  constructor() { }

  ngOnInit() {
  }

}
