import { Input,Component, OnInit } from '@angular/core';
import {ViewEncapsulation} from '@angular/core';
@Component({
  selector: 'generic-component-child',
  templateUrl: './generic-component-child.component.html',
  styleUrls: ['./generic-component-child.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GenericComponentChildComponent implements OnInit {

  @Input() data:string;
  constructor() { }

  ngOnInit() {
  }

}
