import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title:string = 'app';
  info:string = "Text de trimis";
  private _numeVariabile:string = "test";
  private description:string="Here are some links to help you start:";
}
